import OpenAIApi from 'openai';
import TelegramBot, { Message } from 'node-telegram-bot-api';
import * as fs from 'fs';
import { supportedMimeTypes } from "./Config.js";
import { FsReadStream } from "openai/_shims/node-types";

// @ts-ignore
// import pkg from 'telegramify-markdown';
// const { MarkdownTokenizer } = pkg;

import MarkdownTokenizer from 'telegramify-markdown';

const chatGptToken: string = process.env.CHAT_GPT_TOKEN as string;
const telegramBotToken: string = process.env.TELEGRAM_BOT_TOKEN as string;
const chatStateFile: string = '/var/tmp/chatState.json'; // File to store and read the chat ID

interface TgMessageWithChatCompletion {
  tgMessageIds: number[],
  completion: OpenAIApi.Chat.Completions.ChatCompletionMessageParam
}

interface MessageThread {
  threadId: number,
  completions: TgMessageWithChatCompletion[],
}

interface State {
  chatId: number;
  chatCompletions: Array<MessageThread>;
}

let state: State = {
  chatId: NaN,
  chatCompletions: []
};

const maxMessageLength = 4096;

const bot: TelegramBot = new TelegramBot(telegramBotToken, {polling: true});
// Initialize OpenAI
const openai = new OpenAIApi({
  apiKey: chatGptToken, // This is the default and can be omitted
});

function getMessageThread(messageThreadId: number): MessageThread {
  let messageThread = state
    .chatCompletions
    .find((item) => item.threadId === messageThreadId);
  if (messageThread === undefined) {
    const length = state.chatCompletions.push({
      threadId: messageThreadId,
      completions: [],
    });
    messageThread = state.chatCompletions[length - 1];
  }
  return messageThread;
}

function splitEndSendMessage(messageThreadId: number, chatId: number, message: string) {
  const elements = MarkdownTokenizer(message);
  let part = '';
  const tgMessages = [];
  for(const element of elements) {
    if((part.length + element.content.length) > maxMessageLength) {
      tgMessages.push(sendMessage(messageThreadId, chatId, part));
      part = element.content;
    } else {
      part+=element.content;
    }
  }
  if(part.length > 0) tgMessages.push(sendMessage(messageThreadId, chatId, part));
  return tgMessages;
}

function sendMessage(messageThreadId: number, chatId: number, message: string) {
  return bot.sendMessage(
    chatId,
    message,
    {
      message_thread_id: messageThreadId,
      parse_mode: "Markdown",
    }
  );
}

function replyWithOpenAI(msg: Message): Promise<OpenAIApi.Chat.Completions.ChatCompletionMessageParam> {
  return new Promise((resolve, reject) => {
    if (msg.document?.mime_type && (
      msg.document?.mime_type.startsWith('text/') ||
      supportedMimeTypes.includes(msg.document?.mime_type)
    )
    ) {
      bot
        .downloadFile(msg.document.file_id, '/var/tmp/')
        .then((filePath) => {
          const fileStream: FsReadStream = fs.createReadStream(filePath);
          openai.files.create({
            file: fileStream,
            purpose: "assistants",
          })
            .then((fileObject) => {
              console.log(fileObject)
            })

          fs.rm(filePath, () => undefined);
        })
    } else if (msg.text) {
      const chatMsg: OpenAIApi.Chat.Completions.ChatCompletionMessageParam = {
        role: 'user',
        content: msg.text,
      };
      const messageThread = getMessageThread(msg.message_thread_id ?? 0);
      messageThread.completions.push({
        completion: chatMsg,
        tgMessageIds: [msg.message_id ?? 0]
      });
      fs.writeFileSync(chatStateFile, JSON.stringify(state));
      try {
        const chatCompletion = openai.chat.completions.create({
            messages: messageThread.completions.map((item) => item.completion),
            model: 'gpt-4o',
          }
        );
        chatCompletion.then(response => {
          if (response && response.choices.length > 0 && response.choices[0].message) {
            const reponseMsg =
              response.choices[0].message;
            resolve(reponseMsg);
          } else {
            reject('Sorry, I could not find a suitable response.');
          }
        }).catch((error) => {
          console.error('OpenAI Error:', error);
        });
      } catch (error) {
        console.error('OpenAI Error:', error);
        reject('Sorry, I could not process your request.');
      }
    } else {
      reject('Sorry, I could send empty message.');
    }
  });
}

function logMessageFromTopic(message: TelegramBot.Message): void {
  if (message.message_thread_id) {
    console.log(`Message from TopicID ${message.message_thread_id}, msgId ${message.message_id}: ${message.text}`);
  }
}

// Attempt to load chat ID from file
if (fs.existsSync(chatStateFile)) {
  state = JSON.parse(fs.readFileSync(chatStateFile, 'utf8'));
  console.log(`Loaded config from file: ${state}`);
}

function sendTyping(msg: Message) {
  bot.sendChatAction(
    msg.chat.id,
    "typing",
    {
      message_thread_id: msg.message_thread_id
    }
  )
}


function processTgMesage(msg: Message) {
  if ((msg.text || msg.document) && msg.message_thread_id) {
    const messageThread = getMessageThread(msg.message_thread_id)
    const messageAlreadyProcessed = messageThread.completions.find((item) => item.tgMessageIds.find(item => item === msg.message_id))
    if (!messageAlreadyProcessed) {
      sendTyping(msg)
      const typingInterval = setInterval(() =>
        sendTyping(msg), 5000
      );
      replyWithOpenAI(msg)
        .then(response => {
          clearInterval(typingInterval);
          const message = (response as OpenAIApi.Chat.Completions.ChatCompletionAssistantMessageParam).content ?? "Empty response";
          const tgMessages: Promise<TelegramBot.Message>[] = splitEndSendMessage(msg.message_thread_id ?? 0, msg.chat.id, message)
          Promise
            .all(tgMessages)
            .then((responseMessages) => {
              const tgMessageIds = responseMessages.map(item => item.message_id);
              messageThread.completions.push({
                completion: response,
                tgMessageIds: tgMessageIds
              });
              fs.writeFileSync(chatStateFile, JSON.stringify(state));
            });
        })
        .catch(error => {
          console.error('Failed to get response from OpenAI:', error);
          bot.sendMessage(msg.chat.id, 'Sorry, I could not process your request.');
        });
      logMessageFromTopic(msg);
    }
  }
}

bot.on('message', (msg: Message) => {
  // Check if the bot was added to a group
  if (isNaN(state.chatId)) {
    state.chatId = msg.chat.id;
    fs.writeFileSync(chatStateFile, JSON.stringify(state));
    console.log(`Bot added to group, chat ID saved: ${state.chatId}`);
  }

  if (isNaN(state.chatId) || msg.chat.id !== state.chatId) {
    return;
  }
  processTgMesage(msg);
});

bot.on('edited_message', (msg) => {
  if (isNaN(state.chatId) || msg.chat.id !== state.chatId || msg.message_thread_id === undefined) {
    return;
  }
  const messageThread = getMessageThread(msg.message_thread_id);

  const spliceIndex = messageThread.completions.findIndex((item) => item.tgMessageIds.find(item => item === msg.message_id));
  // Find completions with tgMessageId greater than msg.message_id
  const deleteCompletions = messageThread.completions.splice(spliceIndex)

  deleteCompletions.forEach((delCompletion) => {
    // Call bot.deleteMessage() for each of the filtered completions
    Promise.all(delCompletion.tgMessageIds.map(item =>
      bot.deleteMessage(msg.chat.id, item)
        .catch((error) => console.error('Failed to delete message:', error))
    ))
      .then(() => {
        messageThread.completions = messageThread.completions.splice(0, spliceIndex - 1)
      })
  });
  processTgMesage(msg);
});
