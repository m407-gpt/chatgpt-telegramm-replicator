export const supportedMimeTypes = [
  'text/x-log', 'text/x-patch',
  'text/x-c', 'text/x-csharp', 'text/x-c++',
  'application/csv', 'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'text/html', 'text/x-java', 'application/json',
  'text/markdown', 'application/pdf', 'text/x-php',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'text/x-python', 'text/x-script.python', 'text/x-ruby',
  'text/x-tex', 'text/plain', 'text/css',
  'image/jpeg', 'text/javascript', 'image/gif',
  'image/png', 'application/x-sh', 'application/x-tar',
  'application/typescript',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/xml', 'text/xml', 'application/zip'
];