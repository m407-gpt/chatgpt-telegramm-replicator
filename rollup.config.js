import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';

export default {
    input: 'src/server.ts',
    output: {
        file: 'build/server.cjs',
        format: 'cjs',
        exports: 'auto'
    },
    plugins: [
        typescript(),
        json(),
        resolve({
            mainFields: ['module', 'main', 'browser'],
            extensions: [".js", ".ts"],
            preferBuiltins: true,
            jsnext: true,
            main: true,
            browser: false
        }),
        commonjs(), // This must be after resolve plugin
        terser(),
    ]
};